<?php

class demoDataFooter
{
    public function initData()
    {
        $return = true;
        $languages = Language::getLanguages(true);
        $id_shop = Context::getContext()->shop->id;
        $id_hook_footer = (int)Hook::getIdByName('displayFooter');
        $id_hook_footerafter = (int)Hook::getIdByName('displayFooterAfter');
        
        $queries = [
            'INSERT INTO `'._DB_PREFIX_.'posstaticfooter` (`id_posstaticfooter`, `id_hook`, `position`, `width`, `type_content`, `name_module`, `content`,`active_title`) VALUES
                (1, '.$id_hook_footer.', 1, 12, 1, 0, \'{"cms":[false],"product":[false],"static":[false]}\', 0),
                (2, '.$id_hook_footer.', 2, 3, 0, 0, \'{"cms":["1","4","5"],"product":[false],"static":["contact","sitemap","stores"]}\', 1),
				(3, '.$id_hook_footer.', 3, 3, 0, 0, \'{"cms":["2"],"product":["prices-drop","new-products","best-sales"],"static":["authentication","my-account"]}\', 1),
				(4, '.$id_hook_footer.', 4, 3, 1, \'ps_customeraccountlinks\', \'{"cms":[false],"product":[false],"static":[false]}\', 1),
				(5, '.$id_hook_footer.', 5, 3, 1, 0, \'{"cms":[false],"product":[false],"static":[false]}\', 1),
				(6, '.$id_hook_footerafter.', 6, 6, 1, 0, \'{"cms":[false],"product":[false],"static":[false]}\', 0),
				(7, '.$id_hook_footerafter.', 7, 6, 1, 0, \'{"cms":[false],"product":[false],"static":[false]}\', 0);'
        ];

        foreach (Language::getLanguages(true, Context::getContext()->shop->id) as $lang) {
            $queries[] = 'INSERT INTO `'._DB_PREFIX_.'posstaticfooter_lang` (`id_posstaticfooter`, `id_lang`, `name`, `html_content`, `custom_content`) VALUES
                (1, '.(int)$lang['id_lang'].', "Static Info", \'<div class="static-info">
				<div class="col">
				<div class="info info1">
				<div class="icon-static icon ion-ios-location-outline"></div>
				<div class="txt_info">
				<h4>4710-4890 Breckinridge St, USK</h4>
				<span>Contact Info!</span></div>
				</div>
				</div>
				<div class="col">
				<div class="info info2">
				<div class="icon-static icon ion-ios-email-outline"></div>
				<div class="txt_info">
				<h4>demo@posthemes.com</h4>
				<span>Orders Support!</span></div>
				</div>
				</div>
				<div class="col">
				<div class="info info3">
				<div class="icon-static icon ion-ios-telephone-outline"></div>
				<div class="txt_info">
				<h4>+1 (123) 888 9999, +1 (678) 777 8888</h4>
				<span>Free support line!</span></div>
				</div>
				</div>
				</div>\', ""),
                (2, '.(int)$lang['id_lang'].', "Information", "", ""),
                (3, '.(int)$lang['id_lang'].', "Products", "", ""),
				(4, '.(int)$lang['id_lang'].', "My Account", "", ""),
				(5, '.(int)$lang['id_lang'].', "Useful Links", \'
					<li><a href="#">Terms & Conditions</a></li> 
					<li><a href="#">Exchanges</a> <li>
					<li><a href="#">Privacy Policy</a><li> 
					<li><a href="#">Help</a> 
					<li><a href="#">Customer Service</a><li> 
					<li><a href="#">Support</a><li>
				\', ""),
				(6, '.(int)$lang['id_lang'].', "Copyright Block", \'<div class="copyright">Copyright <i class="fa fa-copyright"></i> <a href="http://posthemes.com/">Posthemes</a>. All Rights Reserved</div>\', ""),
				(7, '.(int)$lang['id_lang'].', "Payment Block", \'<div class="payment"><img src="/pos_drama/img/cms/payment.png" alt="" class="img-responsive"></div>\', "")'
				
            ;
        }

        $queries[] = 'INSERT INTO `'._DB_PREFIX_.'posstaticfooter_shop` (`id_posstaticfooter`, `id_shop`) VALUES
                (1, 1),
                (2, 1),
                (3, 1),
                (4, 1),
                (5, 1),
                (6, 1),
                (7, 1)';

        foreach ($queries as $query) {
            $return &= Db::getInstance()->execute($query);
        }

        return $return;
    }
}
?>